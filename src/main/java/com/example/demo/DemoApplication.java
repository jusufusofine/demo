package com.example.demo;

import com.example.demo.user.User;
import com.example.demo.user.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"com.example.demo"})
@SpringBootApplication

public class DemoApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {

	userRepository.save(new User("Markus","Weissenbach","weissenbach.m@gmx.at",""));
	userRepository.save(new User("Christina","Weissenbach","chissi.w@gmx.at",""));

	}

}
