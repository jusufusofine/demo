package com.example.demo.user;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.LoggerFactory;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("api/")
public class UserEndpoint{
    
    private static final Logger logger= LoggerFactory.getLogger(UserEndpoint.class);

    @Autowired
    private UserRepository userRepository;

    @GetMapping("users")
    public ResponseEntity<List<User>> getUsers(){
        return new ResponseEntity<>(userRepository.findAll(),HttpStatus.OK);
    }

    @PostMapping("users")
    public ResponseEntity<User> postUser(@RequestBody User user){
        try{
            userRepository.save(user);
            return new ResponseEntity<>(user,HttpStatus.CREATED);
        }
        catch(Exception e){
          logger.error("Could not add new user", e.fillInStackTrace());
          return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
         
    } 

    @PutMapping("users/{id}")
    public ResponseEntity<User> putUser(@PathVariable("id")Long id, @RequestBody User user){
        System.out.println("Put User was called");
        System.out.println("User:"+user);
        try{

            User storedUser = userRepository.findById(id).get();
            storedUser.setFirstName(user.getFirstName());
            storedUser.setLastName(user.getLastName());
            storedUser.setEmail(user.getEmail());
           
            userRepository.save(storedUser);
            return new ResponseEntity<>(null,HttpStatus.OK);

        }catch(Exception e){

            logger.error("Could not edit user",e.fillInStackTrace());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        }

    }

    @DeleteMapping("users/{id}")
    public ResponseEntity<User> putUser(@PathVariable("id") long id){  
        
        try {
            userRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.OK);

        } catch (Exception e) {
            logger.error("Could not edit user",e.fillInStackTrace());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        }
     
    }

}
